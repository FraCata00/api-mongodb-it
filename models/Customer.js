const mongoose = require('mongoose');
const customerSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Customer', customerSchema);